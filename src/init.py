#!/usr/bin/python

import prometheus_client
import chiafarmer_adapter
import time
import config
import sys
import getopt


def main(argv):
	port = 8000
	try:
		opts, args = getopt.getopt(argv, 'p:', ['port='])
	except getopt.GetoptError:
		print('init.py -p <port>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ('-p', '-port'):
			port = arg

	# expose server
	prometheus_client.start_http_server(int(port))
	print(f'Started server on port {port}.')

	chia_adapter = chiafarmer_adapter.ChiafarmerAdapter()

	while True:
		chia_adapter.collect_farmer_metrics()
		# nbminer_adapter.collect_and_convert_metrics()
		time.sleep(config.interval)


if __name__ == '__main__':
	main(sys.argv[1:])

