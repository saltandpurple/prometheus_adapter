# -*- coding: utf-8 -*-ä
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import requests.exceptions
from prometheus_client import Gauge, Enum
from os.path import expanduser
from typing import final, Callable
from math import pow
from functools import reduce
import json
import config
import datetime


class ChiafarmerAdapter:
	BLOCKTIME_MINUTES: final = 24 * 60 / 4608

	def __init__(self):
		requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

		self.plots_farmed = Gauge('plots_farmed', 'The number of plots currently being farmed')
		self.sync_status_enum = Enum('sync_state', 'The sync state of the local node', states=['syncing', 'synced', 'not_syncing'])
		self.coins_farmed = Gauge('coins_farmed', 'The amount of XCH received as a farming reward')
		self.time_without_win = Gauge('time_without_win', 'The time in minutes since the last height won')
		self.netspace = Gauge('netspace', 'Total network space in TiB')
		self.farmer_space = Gauge('farmer_space', 'Total size of farmed plots in TiB')

	def collect_farmer_metrics(self):
		# find log, check for warn & err -> count / hour (check for "right" warnings)

		# number of plots & owned space (Harvester)
		ChiafarmerAdapter.retrieve_api_response('harvester', 'https://localhost:8560/get_plots', lambda response: self.set_harvester_metrics(response))

		# sync status & netspace ( Full Node )
		blockchain_state: dict = ChiafarmerAdapter.retrieve_api_response('full_node', 'https://localhost:8555/get_blockchain_state',
		                                                                 lambda response: self.set_full_node_metrics(response))

		# farmed coins (wallet)
		farmed_amount: dict = ChiafarmerAdapter.retrieve_api_response('wallet', 'https://localhost:9256/get_farmed_amount',
		                                                              lambda response: self.coins_farmed.set(response['farmed_amount']))

		# time since last win
		if blockchain_state is not None and farmed_amount is not None:
			minutes_since_win = (blockchain_state['blockchain_state']['peak']['height'] - farmed_amount['last_height_farmed']) * self.BLOCKTIME_MINUTES
			self.time_without_win.set(minutes_since_win)


	def set_harvester_metrics(self, response: dict) -> None:
		self.plots_farmed.set(len(response['plots']))
		# map plots to size and then add the size up via reduce
		plots_to_size = map(lambda plot: plot['file_size'], response['plots'])
		plot_size_total = reduce(lambda a, b: a + b, plots_to_size, 0)
		self.farmer_space.set(plot_size_total / pow(1024, 4))  # convert to TiB


	def set_full_node_metrics(self, response: dict) -> None:
		# sync state
		if not response["blockchain_state"]["sync"]["sync_mode"]:
			self.sync_status_enum.state('not_syncing')
		elif response["blockchain_state"]["sync"]["sync_mode"] and not response["blockchain_state"]["sync"]["synced"]:
			self.sync_status_enum.state('syncing')
		else:
			self.sync_status_enum.state('synced')

		# netspace in TiB
		self.netspace.set(response['blockchain_state']['space'] / pow(1024, 4))


	@staticmethod
	def retrieve_api_response(module: str, url: str, result_handler: Callable[[dict], None]) -> dict:
		try:
			cert = config.chiacert
			home = expanduser("~")
			cert[0] = cert[0].format(home=home, module=module)
			cert[1] = cert[1].format(home=home, module=module)
			headers = {'Content-Type': 'application/json'}
			response = requests.post(url, data="{}", cert=cert, headers=headers, verify=False)
			response_decoded = json.loads(response.content)
			result_handler(response_decoded)
			return response_decoded
		except requests.exceptions.ConnectionError:
			print(f'Exception while trying to get data from {module}')
			pass
